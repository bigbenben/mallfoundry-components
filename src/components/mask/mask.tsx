import { ITouchEvent, View } from "@tarojs/components"
import * as React from "react"
import { prefixClassName } from "../../styles"
import "./mask.scss"

interface MaskProps {
  visible?: boolean
  closable?: boolean
  onClose?: (event: ITouchEvent) => void
}

export default function Mask(props: MaskProps) {
  const { visible = false, closable = false, onClose } = props

  function handleClose(event: ITouchEvent) {
    if (closable && onClose) {
      onClose(event)
    }
  }

  return visible ?
    <View
      className={prefixClassName("mask")}
      onClick={handleClose}
    />
    : <React.Fragment />
}
