import ProductList from "./product-list"

export { default as ProductCard } from "./product-card"

export default ProductList
