import { View } from "@tarojs/components"
import * as React from "react"
import ProductCard from "./product-card"
import "./product-list.scss"

interface ProductCardWrapperProps {
  imageUrl: string
  title: string
}

function ProductCardWrapper(props: ProductCardWrapperProps) {
  const { imageUrl, title } = props
  return (
    <View className="product-card-wrapper">
      <ProductCard
        id="{}"
        imageUrl={imageUrl}
        title={title}
      />
    </View>
  )
}

interface ProductListProps {

}

export default function ProductList() {

  return (
    <>
      <View className="product-list">
        <View className="product-list-column2">
          <ProductCardWrapper
            imageUrl="https://resource.smartisan.com/resource/de1274f4c70fe3768417bb0454320089.png"
            title="2020年足迹系列卫衣 人类首次突破音速"
          />
          <ProductCardWrapper
            imageUrl="https://resource.smartisan.com/resource/25574295ceee9802aca571ac57876a1c.png"
            title="2020年足迹系列卫衣 人类首次突破音速"
          />
          <ProductCardWrapper
            imageUrl="https://resource.smartisan.com/resource/de1274f4c70fe3768417bb0454320089.png"
            title="2020年足迹系列卫衣 人类首次突破音速"
          />
          <ProductCardWrapper
            imageUrl="https://resource.smartisan.com/resource/25574295ceee9802aca571ac57876a1c.png"
            title="2020年足迹系列卫衣 人类首次突破音速"
          />
        </View>
        <View className="product-list-column2">
          <ProductCardWrapper
            imageUrl="https://resource.smartisan.com/resource/25574295ceee9802aca571ac57876a1c.png"
            title="2020年足迹系列卫衣 人类首次突破音速"
          />
          <ProductCardWrapper
            imageUrl="https://resource.smartisan.com/resource/4c8b4485240d7ff1cd472030366093d4.jpg"
            title="2020年足迹系列卫衣 人类首次突破音速"
          />
          <ProductCardWrapper
            imageUrl="https://resource.smartisan.com/resource/de1274f4c70fe3768417bb0454320089.png"
            title="2020年足迹系列卫衣 人类首次突破音速"
          />
        </View>
      </View>
      <View style="height:60.52px" />
    </>
  )
}
