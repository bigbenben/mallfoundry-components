export function prefixClassName(name: string) {
  return `omf-${name}`
}
